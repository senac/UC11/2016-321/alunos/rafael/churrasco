package br.com.senac.syschurrasco.activity;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.senac.syschurrasco.R;
import br.com.senac.syschurrasco.model.ItemAtendimento;
import br.com.senac.syschurrasco.model.ItemTabelaVenda;
import br.com.senac.syschurrasco.model.Produto;
import br.com.senac.syschurrasco.model.TabelaVenda;

public class ListaTabelaVendaActivity extends AppCompatActivity {

    public static final String LISTA = "lista";

    private TabelaVenda tabelaVenda = new TabelaVenda();

    private ListView listViewTabelaVenda;

    private ArrayAdapter<ItemTabelaVenda> adapter;

    private List<ItemAtendimento> listaItensSelecionados = new ArrayList<>();

    public ListaTabelaVendaActivity() {
        super();
        initTabelaVenda();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_tabela_venda);

        int layout = android.R.layout.simple_list_item_multiple_choice;

        listViewTabelaVenda = (ListView) findViewById(R.id.listaTabelaVenda);

        adapter = new ArrayAdapter<ItemTabelaVenda>(this, layout, tabelaVenda.getItens());

        listViewTabelaVenda.setAdapter(adapter);

    }


    private void preencherLista() {

        listaItensSelecionados.clear();

        // pegar itens selecionados na listview
        SparseBooleanArray checados = listViewTabelaVenda.getCheckedItemPositions();
        //percorrer a lista de posicoes pegando os itens no adapter e
        // criar novo itens e colocalos na lista de selecionados
        for (int i = 0; i < checados.size(); i++) {
            ItemAtendimento novoItem = adapter.getItem(i).ConvertToItemAtendimento();
            listaItensSelecionados.add(novoItem);

            /*
            ItemTabelaVenda itemTabelaVenda =  adapter.getItem(i);
            ItemAtendimento novoItem = new ItemAtendimento(itemTabelaVenda.getProduto() , 1 , itemTabelaVenda.getPreco());
            */
            /*ItemAtendimento novoItem =  new ItemAtendimento(
                    adapter.getItem(i).getProduto() ,
                    1 ,
                    adapter.getItem(i).getPreco()
            );*/
        }

    }

    @Override
    public void onBackPressed() {

        //alertar usuario


        if(listViewTabelaVenda.getCheckedItemPositions().size() > 0 ) {
            new AlertDialog.Builder(this)
                    .setTitle("Alerta")
                    .setMessage("Deseja incluir os itens selecionados ?")
                    .setCancelable(false) // se clicar back button nao fecha o dialog
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() { //caso ok add itens
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            concluir();
                        }
                    })
                    .setNegativeButton("Não", new DialogInterface.OnClickListener() { //caso contrario so volta a tela anterior
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .show();
        }else{
            finish();
        }






    }

    private void concluir() {
        Intent intent = new Intent();
        this.preencherLista();
        intent.putExtra(ListaTabelaVendaActivity.LISTA,
                (Serializable) listaItensSelecionados); /// colcar os dados
        setResult(RESULT_OK, intent);

        finish();
    }

    public void salvarItens(View view) {

        concluir();

    }


    private void initTabelaVenda() {
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Alcatra"), 3));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Carne de Sol"), 2.75));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Contra Filé"), 2.75));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Contra Filé com Bacon"), 2.80));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Cupim"), 3.10));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Kafta de Carne"), 2.65));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Medalhão de Picanha"), 13));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Frango"), 2.40));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Meio da Asa"), 2.70));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Coração"), 2.40));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Frango com Bacon"), 2.50));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Linguiça de Frango"), 2.60));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Medalhão de Frango"), 2.70));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Costelinha de Porco"), 2.75));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Lombo com Bacon"), 2.60));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Linguiça Apimentada"), 2.50));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Queijo Provolone"), 3));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Queijo Coalho"), 3));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Camarão"), 15.00));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Pão de Alho Caseiro (5 unidades)"), 11));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Refrigerante"), 4.00));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Cerveja"), 5));
    }
}
