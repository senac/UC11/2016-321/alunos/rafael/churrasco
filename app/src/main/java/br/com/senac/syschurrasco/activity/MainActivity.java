package br.com.senac.syschurrasco.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.syschurrasco.R;
import br.com.senac.syschurrasco.adapter.AdapterAtendimento;
import br.com.senac.syschurrasco.model.Atendimento;
import br.com.senac.syschurrasco.model.Cliente;
import br.com.senac.syschurrasco.model.ItemAtendimento;
import br.com.senac.syschurrasco.model.Produto;

public class MainActivity extends AppCompatActivity {

    private List<Atendimento> listaAberto = new ArrayList<>() ;

    private List<Atendimento> listaFechado = new ArrayList<>() ;

    private List<Atendimento> listaCancelado = new ArrayList<>() ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TabHost host = (TabHost) findViewById(R.id.tabHost) ;
        host.setup();

        //Tab1 - Abertos
        TabHost.TabSpec tabAbertos = host.newTabSpec("Abertos") ;
        tabAbertos.setContent(R.id.Abertos) ;
        tabAbertos.setIndicator("Abertos") ;
        host.addTab(tabAbertos);

        //Tab2 - Fechados
        TabHost.TabSpec tabFechados = host.newTabSpec("Fechados") ;
        tabFechados.setContent(R.id.Fechados) ;
        tabFechados.setIndicator("Fechados") ;
        host.addTab(tabFechados);

        //Tab3 - Cancelados
        TabHost.TabSpec tabCancelados = host.newTabSpec("Cancelados") ;
        tabCancelados.setContent(R.id.Cancelados) ;
        tabCancelados.setIndicator("Cancelados") ;
        host.addTab(tabCancelados);


        Atendimento atendimento = new Atendimento();
        atendimento.setCliente(new Cliente("Daniel"));
        atendimento.add(new ItemAtendimento(new Produto(1 , "Churrasco") , 3 , 3 ));
        listaAberto.add(atendimento);

        AdapterAtendimento adapterAtendimentoAberto = new AdapterAtendimento(this , listaAberto) ;

        ListView listViewAbertos = (ListView) findViewById(R.id.ListaAtendimentosAbertos) ;

        listViewAbertos.setAdapter(adapterAtendimentoAberto);







    }

    @Override public boolean onCreateOptionMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_atendimento_lista,menu);

        return true;

    }
    public  void novo(MenuItem item){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Nome do Cliente");

        EditText input = new EditText(this);

        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener){
        @Override
                public void onClick(DialogInterface dialogInterface, int i)
        }
    }
    private void novoAtendimento(String nome){
        Cliente cliente = new Cliente(nome);

        Intent intent = new Intent(this, AtendimentoActivity.class);
        intent.putExtra(MainActivity.NOVO_ATENDIMENTO, atendimento);
        startActivityForResult(intent, MainActivity.NOVO);

    }
    @Override
    protected void onActivityResult(int resultCode, Intent data);
    if(resultCode == )

}
