package br.com.senac.syschurrasco.model;


import java.io.Serializable;
import java.text.NumberFormat;

public class ItemAtendimento implements Serializable{

    private NumberFormat nf = NumberFormat.getCurrencyInstance();
    private Produto produto;
    private int quantidade;
    private double preco;

    public ItemAtendimento() {
    }

    public ItemAtendimento(Produto produto, int quantidade, double preco) {
        this.produto = produto;
        this.quantidade = quantidade;
        this.preco = preco;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPreco() {
        return preco;
    }

    public String getPrecoFormatado(){
        return nf.format(this.preco) ;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getTotal() {
        return this.quantidade * this.preco;
    }

    public String getTotalFormatado(){
        return nf.format(this.getTotal());
    }
}

