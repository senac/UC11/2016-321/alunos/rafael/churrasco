package br.com.senac.syschurrasco.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import br.com.senac.syschurrasco.R;
import br.com.senac.syschurrasco.adapter.AdapterItemAtendimento;
import br.com.senac.syschurrasco.model.Atendimento;
import br.com.senac.syschurrasco.model.ItemAtendimento;
import br.com.senac.syschurrasco.model.Produto;

public class AtendimentoActivity extends AppCompatActivity {

    public static final int ADD_ITEM = 1;
    private Atendimento atendimento;

    private AdapterItemAtendimento adapter;
    private TextView textViewNomeCliente;
    private ListView listViewItensAtendimento;
    private TextView textViewTotalAtendimento;

    public AtendimentoActivity() {
        this.init();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atendimento);


        textViewNomeCliente = (TextView) findViewById(R.id.nome_cliente);
        listViewItensAtendimento = (ListView) findViewById(R.id.lista_itens_atendimento);
        textViewTotalAtendimento = (TextView) findViewById(R.id.total_atendimento);


        adapter = new AdapterItemAtendimento(this, atendimento.getItens());

        listViewItensAtendimento.setAdapter(adapter);

        atualizarTela();


        listViewItensAtendimento.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapter, View contexto, int posicao, long l) {


                final ItemAtendimento item = (ItemAtendimento)adapter.getItemAtPosition(posicao);



                RelativeLayout linearLayout = new RelativeLayout(AtendimentoActivity.this) ;


                final NumberPicker numberPicker = new NumberPicker(AtendimentoActivity.this) ;
                numberPicker.setMinValue(item.getQuantidade());
                numberPicker.setMaxValue(50);
                //settar a quantidade no numberpicker
                numberPicker.setValue(item.getQuantidade());


                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50 , 50) ;
                RelativeLayout.LayoutParams numberPickerParams = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT ,
                        RelativeLayout.LayoutParams.WRAP_CONTENT
                        );
                numberPickerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

                linearLayout.setLayoutParams(params);
                linearLayout.addView(numberPicker , numberPickerParams);


                final AlertDialog.Builder dialog = new AlertDialog.Builder(AtendimentoActivity.this)  ;
                dialog.setTitle("Selecione a quantiade") ;
                dialog.setView(linearLayout) ;
                dialog.setCancelable(false) ;
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ///alterar a quantidade
                        item.setQuantidade(numberPicker.getValue());
                        atualizarTela();

                    }
                }) ;
                dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel() ;
                    }
                }) ;

                AlertDialog alertDialog = dialog.create() ;
                alertDialog.show();




               /* Toast.makeText( AtendimentoActivity.this ,
                                "Clicou na posicao " + posicao ,
                        Toast.LENGTH_LONG )
                        .show();
                        */

                return false;
            }
        });




    }

    private void atualizarTela() {


        adapter.notifyDataSetChanged();

        textViewNomeCliente.setText(atendimento.getNomeCliente());

        textViewTotalAtendimento.setText(atendimento.getTotalFormatado());


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_atendimento, menu);

        return true;

    }

    public void add(MenuItem item) {
        //de onde estou pra onde vou .....
        Intent intent = new Intent(this, ListaTabelaVendaActivity.class);
        // startActivity(intent); --- nao passa na volta por onActivityResult

        // starto a activity em quero utilizar o metodo onActivityResult de callback
        startActivityForResult(intent, ADD_ITEM);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

       /// Toast.makeText(this, " QTA " + atendimento.getItens().size(), Toast.LENGTH_LONG).show();

        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_ITEM) {
                //pegar atendimento e colocar novos itens .....
                List<ItemAtendimento> lista = (List<ItemAtendimento>) data.getSerializableExtra(ListaTabelaVendaActivity.LISTA);
                for (ItemAtendimento item : lista) {
                    atendimento.add(item);
                }

                //atualizar a tela
                atualizarTela();



               /*
                for(int i = 0 ; i < lista.size() ; i++){
                    ItemAtendimento item =  lista.get(i) ;
                    atendimento.add(item);
                }
                */


            }
        }

       // Toast.makeText(this, " QRU " + atendimento.getItens().size(), Toast.LENGTH_LONG).show();

      //  Toast.makeText(this, " codigo " + resultCode, Toast.LENGTH_LONG).show();

    }

    public void init() {


        this.atendimento = new Atendimento("Alann");
        this.atendimento.add(new ItemAtendimento(new Produto(1, "Churraquinho1"), 2, 5));
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(1, "Churraquinho2"), 2, 7));
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(2, "Churraquinho3"), 3, 8));
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(3, "Churraquinho4"), 5, 5));
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(4, "Churraquinho5"), 7, 3));
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(5, "Churraquinho6"), 1, 2));

    }


}
